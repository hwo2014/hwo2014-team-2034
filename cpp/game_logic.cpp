#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

std::string my_name = "sac";
game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  my_name = data["name"].as<std::string>();
  std::cout << "my_name is: '" << my_name << "'" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

namespace piece {
  enum _enum {
    Straight,
    Curve
  };
}
struct Piece {
  piece::_enum type;
  bool can_switch;
  float length;
  float radius;
  float angle;
};

std::vector<Piece> pieces;

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "Race init" << std::endl;
  // jsoncons::json::array track = data["race"]["track"].as<jsoncons::json::array>();
  auto _pieces = data["race"]["track"]["pieces"];
  for (int i=0; i<_pieces.size(); i++) {
    Piece p;

    p.length = _pieces[i].get("length", -1).as<double>();
    if (p.length < 0) {
      p.radius = _pieces[i]["radius"].as<double>();
      p.angle = _pieces[i]["angle"].as<double>();
      p.type = piece::Curve;
    } else {
      p.type = piece::Straight;
    }

    pieces.push_back(p);
  }
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

int last_piece = -1;
int logic = -1;
float throttle = 1;
float prev_throttle = 1;
int gaz = 1;
float last_drift;
game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  for (int i=0; i<data.size(); i++) {
    auto d = data[i];
    if (d["id"]["name"].as<std::string>() != my_name)
      continue;

    int piece_index = d["piecePosition"]["pieceIndex"].as<int>();

    if (piece_index != last_piece) {
      last_piece = piece_index;
      std::cout << "New piece: " << last_piece << "(throttle: " << throttle << ')' << std::endl;
      if (pieces[piece_index].type == piece::Straight) {
        std::cout << "   > straight / length: " << pieces[piece_index].length << std::endl;
      } else {
        std::cout << "   > turning / radius: " << pieces[piece_index].radius << std::endl;
        std::cout << "   > turning / angle: " << pieces[piece_index].angle << std::endl;
      }
    }

    const float drift = std::abs(d["angle"].as<double>() * 6.28 / 360.0);

#if 0
    if (pieces[piece_index].type == piece::Straight ) {
      return { make_throttle(1.0) };
    } else {
      if (std::abs(d["angle"].as<double>()) > 0.05)
        return { make_throttle(0.0) };
      else
        return { make_throttle(1.0) };
    }
#endif
#if 1
    if (pieces[piece_index].type == piece::Straight ) {
      float progression = d["piecePosition"]["inPieceDistance"].as<double>() / pieces[piece_index].length;

      const Piece& next_piece = pieces[(piece_index + 1) % pieces.size()];
      if (next_piece.type == piece::Straight ) {
        if (logic != 0) {
          logic = 0;
          std::cout << "GAZ!" << std::endl;
        }
        throttle = 1;
      } else {

        if (std::abs(next_piece.angle) >= 45 ) {
          if (logic != 1) {
            logic = 1;
            std::cout << "PREPARE BIG TURN!" << std::endl;
          }

          float threshold = 0.1;
          if (progression > threshold) {
            throttle = 1.0f - (progression - threshold) * 4.0f;
          } else {
            throttle = 1;
          }
        } else {
          if (logic != 2) {
            logic = 2;
            std::cout << "PREPARE LITTLE TURN!" << std::endl;
          }
          throttle = 1;
        }
      }
    } else {
        if (logic != 3) {
          logic = 3;
          std::cout << "TURN!" << std::endl;
        }

        // keep drift under control
        if (std::abs(drift) < 0.2)
          throttle += 0.1;
        else
          throttle -= 0.1;

      // return { make_throttle(std::max(0.0f, 1 - 3 * std::abs(drift))) };
    }
    last_drift = drift;
#endif
  }

  if (prev_throttle < throttle && !gaz) {
    std::cout << "acceleration "  << prev_throttle << "->" << throttle << std::endl;
    prev_throttle = throttle;
    gaz = 1;
  }

  if (prev_throttle > throttle && gaz) {
    std::cout << "freinage "  << prev_throttle << "->" << throttle << std::endl;
    prev_throttle = throttle;
    gaz = 0;
  }

  throttle = std::min(1.0f, std::max(0.0f, throttle));
  return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  std::cout << "Current throttle: " << throttle << ", drift: " << last_drift << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
